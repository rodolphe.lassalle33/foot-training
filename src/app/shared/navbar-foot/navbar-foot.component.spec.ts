import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarFootComponent } from './navbar-foot.component';

describe('NavbarFootComponent', () => {
  let component: NavbarFootComponent;
  let fixture: ComponentFixture<NavbarFootComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NavbarFootComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NavbarFootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
