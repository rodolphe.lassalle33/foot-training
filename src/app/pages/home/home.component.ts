import { Component } from '@angular/core';
import { NavbarFootComponent } from '../../shared/navbar-foot/navbar-foot.component';

@Component({
  selector: 'foot-home',
  standalone: true,
  imports: [NavbarFootComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {

}
